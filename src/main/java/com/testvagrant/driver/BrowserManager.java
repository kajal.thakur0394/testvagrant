package com.testvagrant.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserManager {
	private static WebDriver driver;

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		BrowserManager.driver = driver;
	}

	public BrowserManager() {
		String browser = System.getProperty("set.Browser");
		switch (browser) {
		case "chrome":
			invokeChrome();
			break;
		case "firefox":
			invokeFirefox();
			break;
		case "safari":
			invokeSafari();
			break;
		default:
			break;
		}
		driver.manage().window().maximize();
	}

	private WebDriver invokeSafari() {
		setDriver(new SafariDriver());
		return getDriver();
	}

	private WebDriver invokeFirefox() {
		setDriver(new FirefoxDriver());
		return getDriver();
	}

	private WebDriver invokeChrome() {
		setDriver(new ChromeDriver());
		return getDriver();
	}

}
