package com.testvagrant.elements;

import org.openqa.selenium.By;

public enum ImdbPageElements {
	SEARCH_SUGGESTION(getId("suggestion-search-button"));
	
	public static String SEARCH_BAR="//input[@placeholder='%s']";
	public static String MOVIE_SEARCH="//ul//*[text()='%s']";
	public static String DATA="//*[text()='%s']//following-sibling::div//a";

	By locator;

	ImdbPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
