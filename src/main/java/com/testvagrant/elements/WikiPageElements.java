package com.testvagrant.elements;

import org.openqa.selenium.By;

public enum WikiPageElements {
	SEARCH_BUTTON(getXpath("//button[text()='Search']")),
	RELEASE_DATE(getXpath("//*[text()='Release date']/..//following-sibling::td//div//li")),
	COUNTRY(getXpath("//*[text()='Country']/..//following-sibling::td"));

	By locator;

	WikiPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
