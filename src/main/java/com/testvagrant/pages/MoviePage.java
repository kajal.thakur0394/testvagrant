package com.testvagrant.pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.testvagrant.elements.ImdbPageElements;
import com.testvagrant.elements.WikiPageElements;
import com.testvagrant.utils.SeleniumUtility;

public class MoviePage extends SeleniumUtility {
	Map<String, String> imdbMap = new HashMap<>();
	Map<String, String> wikiMap = new HashMap<>();

	public void searchAndFetchDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String source = choice.get(0);
			String movieName = choice.get(1);
			String detail = choice.get(2);
			if (("IMDB").equalsIgnoreCase(source)) {
				searchImdbDetails(movieName, detail);
			} else if (("WIKI").equalsIgnoreCase(source)) {
				searchWikiDetails(movieName, detail);
			}
		}

	}

	private void searchWikiDetails(String movieName, String detail) {
		sendKeys(dynamicXpathLocator(ImdbPageElements.SEARCH_BAR, "Search Wikipedia"), movieName);
		click(WikiPageElements.SEARCH_BUTTON.locate());
		if (("Release date").equalsIgnoreCase(detail)) {
			wikiMap.put(detail, fetchText(WikiPageElements.RELEASE_DATE.locate()));
		} else {
			wikiMap.put(detail.concat(" of origin"), fetchText(WikiPageElements.COUNTRY.locate()));
		}

	}

	private void searchImdbDetails(String movieName, String detail) {
		sendKeys(dynamicXpathLocator(ImdbPageElements.SEARCH_BAR, "Search IMDb"), movieName);
		click(ImdbPageElements.SEARCH_SUGGESTION.locate());
		click(dynamicXpathLocator(ImdbPageElements.MOVIE_SEARCH, movieName));
		imdbMap.put(detail, fetchText(dynamicXpathLocator(ImdbPageElements.DATA, detail)));
	}

	public void openSourceUrl(String source) {
		switch (source) {
		case "IMDB":
			openURL("https://www.imdb.com");
			break;
		case "WIKI":
			openURL("https://en.wikipedia.org/wiki/Main_Page");
			break;
		default:
			break;
		}
	}

	public void verifyData(String detail) {
		Assert.assertEquals(imdbMap.get(detail), wikiMap.get(detail));

	}
}
