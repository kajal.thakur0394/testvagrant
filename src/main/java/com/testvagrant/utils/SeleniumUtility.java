package com.testvagrant.utils;

import java.time.Duration;
import java.util.List;
import java.util.Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.testvagrant.driver.BrowserManager;

public class SeleniumUtility {
	private static WebDriver driver;
	private WebDriverWait wait;
	private JavascriptExecutor js;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		SeleniumUtility.driver = driver;
	}

	public SeleniumUtility() {
		if (Objects.isNull(BrowserManager.getDriver())) {
			new BrowserManager();
		}
		setDriver(BrowserManager.getDriver());
		wait = new WebDriverWait(driver, Duration.ofSeconds(45));
		js = (JavascriptExecutor) driver;
	}

	public void openURL(String url) {
		driver.get(url);
	}

	public void click(By by) {
		wait.until(ExpectedConditions.elementToBeClickable(by));
		try {
			driver.findElement(by).click();
		} catch (Exception ElementClickInterceptedException) {
			WebElement element = driver.findElement(by);
			js.executeScript("arguments[0].click();", element);
		}
	}

	public String fetchText(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return driver.findElement(by).getText();
	}

	public void clickUsingJSExecutor(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		WebElement loc = driver.findElement(by);
		js.executeScript("arguments[0].click();", loc);
	}

	public void sendKeys(By by, CharSequence text) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.findElement(by).sendKeys(text);
	}

	public WebElement findElement(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		WebElement element = driver.findElement(by);
		return element;
	}

	public List<WebElement> findElements(By by) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			List<WebElement> elements = driver.findElements(by);
			return elements;
		} catch (Exception e) {
			List<WebElement> elements = driver.findElements(by);
			return elements;
		}
	}
	
	public By dynamicXpathLocator(String element, String value) {
		element = element.replace("%s", value);
		return By.xpath(element);
	}

	public void waitForElementToBeVisible(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForElementToBeInvisible(By by) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public void waitForElementToBePresent(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForElementToBeClickable(By by) {
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

}
