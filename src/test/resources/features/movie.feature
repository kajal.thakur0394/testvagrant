@Movies @Regression
Feature: Movie module

  Scenario: To verify release date of a movie from IMDB and WIKI sources
    Given I navigate to "IMDB" source
    And I search and fetch the following details
      | IMDB | Pushpa: The Rise - Part 1 | Release date |
    Given I navigate to "WIKI" source
    And I search and fetch the following details
      | WIKI | Pushpa: The Rise - Part 1 | Release date |
    Then I verify "Release date" details from both sources

  Scenario: To verify Country of origin of a movie from IMDB and WIKI sources
    Given I navigate to "IMDB" source
    And I search and fetch the following details
      | IMDB | Pushpa: The Rise - Part 1 | Country of origin |
    Given I navigate to "WIKI" source
    And I search and fetch the following details
      | WIKI | Pushpa: The Rise - Part 1 | Country |
    Then I verify "Country of origin" details from both sources
