package com.testvagrant.stepDefinitions;

import java.util.List;

import com.testvagrant.pages.MoviePage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class MovieStepDefinitions {
	MoviePage objectOfMoviePage = new MoviePage();
	

	@Given("I navigate to {string} source")
	public void i_navigate_to_source(String source) {
		objectOfMoviePage.openSourceUrl(source);
	}

	@Given("I search and fetch the following details")
	public void i_search_and_fetch_the_following_details(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		objectOfMoviePage.searchAndFetchDetails(choices);
	}
	@Then("I verify {string} details from both sources")
	public void i_verify_details_from_both_sources(String attribute) {
		objectOfMoviePage.verifyData(attribute);
	}

}
