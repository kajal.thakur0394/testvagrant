
package com.testvagrant.runners;

import java.io.IOException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.testvagrant.driver.BrowserManager;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = { "src/test/resources/features/" }, glue = { "com.testvagrant.stepDefinitions" }, plugin = {
		"pretty", "json:target/cucumber-reports/Cucumber.json",
		"junit:target/cucumber-reports/Cucumber.xml" ,"html:target/cucumber-reports/index.html"}, monochrome = true, tags = "@Regression")
public class TestRunner {

	@BeforeClass
	public static void initialize() throws IOException, InterruptedException {
		/*
		 * Uncomment the below line to execute from here as jUnit TestRunner. If
		 * executing it from maven, comment the line and pass browser in maven command.
		 */
		//System.setProperty("set.Browser", "chrome");
	}

	@AfterClass
	public static void tearDown() throws IOException {
		BrowserManager.getDriver().quit();
	}

}
