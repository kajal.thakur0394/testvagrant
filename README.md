# TestVagrant

# Requirements
In order to setup this project you'll need following things installed and configured on your local machine
* Maven
* Java
* Browsers
    * Chrome
    * Firefox
* Java IDE
    * Cucumber Plugin
# Tech Stack
* Cucumber
* Selenium WebDriver
* JUnit
# Usage
To execute this as a maven project:
* mvn clean compiler:compile
* mvn clean verify -Dset.Browser=“chrome”

To execute this from TestRunner.java:
* Uncomment ‘System.setProperty("set.Browser", "chrome”)’ from TestRunner.java 
* Right click->Run as->Junit

# Report
Basic cucumber report can be found in the following path: 
{projectDirectory}/target/cucumber-reports/index.html
